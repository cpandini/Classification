STXS Classification package
---------------------------

Rivet code implementing the classification of a MC event into STXS bins.
Currently supporting the STXS Stage 1.1 binning defined here: https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGFiducialAndSTXS#Stage_1_1
